# build CSP string from an object

This helper function creates a `Content-Security-Policy` content string from a javascript object.

## usage

E.g.:

```
<meta http-equiv={"Content-Security-Policy"} content={
	buildCsp({
		'script-src':  [ 'self', 'https://www.google-analytics.com' ],
		'style-src':   [ 'self', 'unsafe-inline', 'https://fonts.googleapis.com' ],
		'default-src': [ 'self' ]
	})
} />
```

## intention

Larger `Content-Security-Policy` content strings tend to be hard to read and manage. e.g.:

```
<meta
	http-equiv="Content-Security-Policy"
	content={
		"connect-src 'self' https://example.com; script-src 'self' 'unsafe-eval' 'unsafe-inline';" +
		" font-src 'self'; img-src * blob: data:; object-src 'self' blob:;" +
		" worker-src 'self' 'unsafe-eval' 'unsafe-inline' blob: https://localhost;" +
		" style-src 'self' 'unsafe-inline'; default-src 'self';
	}
/>
```

To keep a better overview and make management of the directives easier, this `buildCsp` function allows you
to write a javascript object instead, and lets you concentrate on the URLs and sources you want to allow,
and not have to care about spaces and semicolons so much, like:

```
<meta
	http-equiv="Content-Security-Policy"
	content={ buildCsp({
		"connect-src":  [ 'self', 'https://example.com' ],
		'script-src':   [ 'self', 'unsafe-eval', 'unsafe-inline' ],
		'font-src':     [ 'self' ],
		'img-src':      [ '*', 'blob:', 'data:' ],
		'object-src':   [ 'self', 'blob:' ],
		'style-src':    [ 'self', 'unsafe-inline' ],
		'default-src':  [ 'self' ],
		'worker-src': [
			'self',
			'unsafe-eval',
			'unsafe-inline',
			'blob:',
			'https://localhost'
		],
	}) }
/>
```

## not implemented yet:

- `nonce-<base64-value>`
- `<hash-algorithm>-<base64-value>`
