"use strict";
// ============================================================================
//
// Convert CSP object to string
//
// usage e.g.:
//   <meta httpEquiv={"Content-Security-Policy"} content={
//     buildCSP({
//       'script-src':  [ 'self', 'https://www.google-analytics.com' ],
//       'style-src':   [ 'self', 'unsafe-inline', 'https://fonts.googleapis.com' ],
//       'default-src': [ 'self' ]
//     })
//   } />
// ============================================================================
Object.defineProperty(exports, "__esModule", { value: true });
exports.buildCsp = exports.listOfDirectives = void 0;
// -- not implemented yet:
// nonce-<base64-value>
// <hash-algorithm>-<base64-value>
// --
exports.listOfDirectives = [
    'child-src',
    'connect-src',
    'default-src',
    'font-src',
    'frame-src',
    'img-src',
    'manifest-src',
    'media-src',
    'object-src',
    'prefetch-src',
    'script-src',
    'script-src-elem',
    'script-src-attr',
    'style-src',
    'style-src-elem',
    'style-src-attr',
    'worker-src',
];
const specialSources = [
    'self',
    'unsafe-inline',
    'unsafe-eval',
    'none',
    'strict-dynamic',
    'report-sample',
];
// ------------------
// functions
// ------------------
const guardValidDirective = function (key) {
    // -- invalid directive key
    if (typeof key !== 'string' || exports.listOfDirectives.indexOf(key) < 0) {
        return false;
    }
    return true;
};
// ------------------
// main
// ------------------
const buildCsp = function (data) {
    // --
    const renderedDirectives = [];
    // --
    Object.keys(data).forEach((key) => {
        // -- invalid directive key
        if (!guardValidDirective(key)) {
            console.warn('Unknown directive:', key);
        }
        const directive = data[key];
        const renderedSources = [];
        // -- invalid directive value
        if (!Array.isArray(directive)) {
            console.error('Invalid directive:', key, ':', typeof directive);
            return;
        }
        // -- sources
        directive.forEach((sourceKey, sourceIndex) => {
            // -- empty value
            if (!sourceKey) {
                console.error('The CSP source in "' + key + '" at index ' + sourceIndex + ' is empty.\n' +
                    'The empty source will be ignored, but this indicates an error in the CSP definition.\n' +
                    'E.g. is some dynamic variable or environment variable not set ?\n', '>>>', key, ':', directive);
                return;
            }
            // -- error (e.g. source === undefined)
            if (typeof sourceKey !== 'string') {
                console.error('CSP source in "', key, '" is ', typeof sourceKey);
                return;
            }
            // --
            let renderedSource;
            if (sourceKey.substring(0, 3) === 'sha') {
                console.warn('Hash values are not properly implemented yet.');
                renderedSource = '\'' + sourceKey + '\'';
            }
            else if (sourceKey.substring(0, 6) === 'nonce-') {
                console.warn('\'nonce-...\' is not properly implemented yet.');
                renderedSource = '\'' + sourceKey + '\'';
            }
            else if (specialSources.indexOf(sourceKey) > -1) {
                renderedSource = '\'' + sourceKey + '\'';
            }
            else {
                // Unexpected source key. Should we warn ?
                renderedSource = sourceKey;
            }
            renderedSources.push(renderedSource);
        });
        renderedDirectives.push(key + ' ' + renderedSources.join(' ') + ';');
    });
    // --
    return renderedDirectives.join(' ');
};
exports.buildCsp = buildCsp;
