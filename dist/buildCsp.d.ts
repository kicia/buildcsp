export declare const listOfDirectives: readonly ["child-src", "connect-src", "default-src", "font-src", "frame-src", "img-src", "manifest-src", "media-src", "object-src", "prefetch-src", "script-src", "script-src-elem", "script-src-attr", "style-src", "style-src-elem", "style-src-attr", "worker-src"];
type ValidDirective = (typeof listOfDirectives)[number];
export type CspData = Partial<Record<ValidDirective, string[]>>;
export declare const buildCsp: (data: CspData) => string;
export {};
