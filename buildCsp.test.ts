// =====================================================================
//
// test buildCsp
//
// =====================================================================

import { describe, expect, test, jest, beforeEach, afterEach } from '@jest/globals';
import { buildCsp } from './buildCsp';

describe( 'buildCsp', function () {

	// --
	const cspStringSnapshot = `connect-src 'self' (middleWareAPI) ` +
		`wss://(middleWareAPI); ` +
		"script-src 'self' 'unsafe-eval' 'unsafe-inline' https://www.google-analytics.com; " +
		"font-src 'self' https://fonts.gstatic.com https://fonts.googleapis.com; " +
		"img-src 'self' blob: https://www.google-analytics.com; " +
		"object-src 'self' blob:; " +
		"worker-src 'self' blob: https://storage.googleapis.com 'unsafe-inline'; " +
		"style-src 'self' 'unsafe-inline' https://fonts.googleapis.com; " +
		"default-src 'self';";

	// --
	const cspData = {
		'connect-src': [ 'self', '(middleWareAPI)', 'wss://(middleWareAPI)' ],
		'script-src': [ 'self', 'unsafe-eval', 'unsafe-inline', 'https://www.google-analytics.com' ],
		'font-src': [ 'self', 'https://fonts.gstatic.com', 'https://fonts.googleapis.com' ],
		'img-src': [ 'self', 'blob:', 'https://www.google-analytics.com' ],
		'object-src': [ 'self', 'blob:' ],
		'worker-src': [ 'self', 'blob:', 'https://storage.googleapis.com', 'unsafe-inline' ],
		'style-src': [ 'self', 'unsafe-inline', 'https://fonts.googleapis.com' ],
		'default-src': [ 'self' ]
	};

	const errorMessageEmptyCspSource = 'The CSP source in "connect-src" at index 1 is empty.\n' +
		'The empty source will be ignored, but this indicates an error in the CSP definition.\n' +
		'E.g. is some dynamic variable or environment variable not set ?\n';

	// --
	const data = [
		{
			title: 'some urls',
			value: { 'connect-src': [ 'self', 'https://www.url.net', 'wss://www.url.net' ] },
			expected: "connect-src 'self' https://www.url.net wss://www.url.net;"
		},
		{
			title: 'special sources',
			value: { 'connect-src': [ 'self', 'unsafe-inline', 'unsafe-eval', 'none', 'strict-dynamic', 'report-sample' ] },
			expected: "connect-src 'self' 'unsafe-inline' 'unsafe-eval' 'none' 'strict-dynamic' 'report-sample';"
		},
		{
			title: 'multi directives',
			value: {
				'connect-src': [ 'http://abc', 'self' ],
				'font-src': [ 'abc', 'self' ],
				'worker-src': [ 'xyz', 'self' ]
			},
			expected: "connect-src http://abc 'self'; font-src abc 'self'; worker-src xyz 'self';"
		},
		{
			title: 'wrong directive',
			value: { 'xyz-src': [ 'self', 'http://abc' ] },
			expected: "xyz-src 'self' http://abc;",
			shouldWarn: 'Unknown directive:'
		},
		{
			title: 'empty value',
			value: { 'connect-src': [ 'http://abc', '', 'self' ], 'worker-src': [ 'xyz', '', 'self' ] },
			expected: "connect-src http://abc 'self'; worker-src xyz 'self';",
			shouldError: errorMessageEmptyCspSource
		},
		{
			title: 'nonce',
			value: { 'connect-src': [ 'nonce-12345' ] },
			expected: "connect-src 'nonce-12345';",
			shouldWarn: '\'nonce-...\' is not properly implemented yet.'
		}, // TODO: check/implement properly
		{
			title: 'sha',
			value: { 'connect-src': [ 'sha512-12345' ] },
			expected: "connect-src 'sha512-12345';",
			shouldWarn: 'Hash values are not properly implemented yet.'
		}, // TODO: check/implement properly
		{ title: 'snapshot', value: cspData, expected: cspStringSnapshot }
	];

	const originalConsole = {
		log: console.log,
		error: console.error,
		warn: console.warn
	};

	beforeEach( () => {
		console.warn = jest.fn( ( value: string ) => {
			return value;
		} );
		console.error = jest.fn( ( value: string ) => {
			return value;
		} );
	} );

	afterEach( () => {
		console.warn = originalConsole.warn;
		console.error = originalConsole.error;
	} );

	// --
	data.forEach( function ( testItem ) {
		test( testItem.title, () => {
			const result = buildCsp( testItem.value );
			expect( result ).toBe( testItem.expected );
			if ( testItem.shouldWarn ) {
				expect( console.warn ).toHaveBeenCalledTimes( 1 );
				expect( console.warn ).toHaveNthReturnedWith( 1, testItem.shouldWarn );
			} else {
				expect( console.warn ).toHaveBeenCalledTimes( 0 );
			}
			if ( testItem.shouldError ) {
				// expect(console.error).toHaveBeenCalledTimes(1);
				expect( console.error ).toHaveNthReturnedWith( 1, testItem.shouldError );
			} else {
				expect( console.error ).toHaveBeenCalledTimes( 0 );
			}
		} );
	} );
} );
